<?php

namespace DocumentManager\BackendBundle\Controller\Document;

use Admingenerated\DocumentManagerBackendBundle\BaseDocumentController\ShowController as BaseShowController;

/**
 * ShowController
 */
class ShowController extends BaseShowController
{
    public function getUsersByDocumentAction($document){
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('DocumentManagerBackendBundle:DocumentUser')->findByDocument($document);
        return $this->render('DocumentManagerBackendBundle:DocumentShow:_users.html.twig', array('users' => $users));
    }
}
