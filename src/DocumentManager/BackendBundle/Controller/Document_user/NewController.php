<?php

namespace DocumentManager\BackendBundle\Controller\Document_user;

use Admingenerated\DocumentManagerBackendBundle\BaseDocument_userController\NewController as BaseNewController;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * NewController
 */
class NewController extends BaseNewController {

    public function createAction() {
        $DocumentUser = $this->getNewObject();

        $this->preBindRequest($DocumentUser);
        $form = $this->createForm($this->getNewType(), $DocumentUser, $this->getFormOptions($DocumentUser));
        $form->bind($this->get('request'));

        if ($form->isValid()) {
            try {
                $this->preSave($form, $DocumentUser);
                if ($this->getRequest()->get('document_id')) {
                    $document_id = $this->getRequest()->get('document_id');
                    $em = $this->getDoctrine()->getManager();
                    $document = $em->getRepository('DocumentManagerBackendBundle:Document')->find($document_id);
                    $DocumentUser->setDocument($document);
                }
                
                $this->saveObject($DocumentUser);
                $this->postSave($form, $DocumentUser);

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans("action.object.edit.success", array(), 'Admingenerator'));

                if ($this->get('request')->request->has('save-and-add'))
                    return new RedirectResponse($this->generateUrl("DocumentManager_BackendBundle_Document_user_new", array('document_id' => $document_id)));
                if ($this->get('request')->request->has('save-and-list'))
                    return new RedirectResponse($this->generateUrl("DocumentManager_BackendBundle_Document_user_list", array('document_id' => $document_id)));
                else
                    return new RedirectResponse($this->generateUrl("DocumentManager_BackendBundle_Document_user_edit", array('pk' => $DocumentUser->getId(), 'document_id' => $document_id)));
            } catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator'));
                $this->onException($e, $form, $DocumentUser);
            }
        } else {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator'));
        }

        return $this->render('DocumentManagerBackendBundle:Document_userNew:index.html.twig', $this->getAdditionalRenderParameters($DocumentUser) + array(
                    "DocumentUser" => $DocumentUser,
                    "form" => $form->createView(),
        ));
    }

}
