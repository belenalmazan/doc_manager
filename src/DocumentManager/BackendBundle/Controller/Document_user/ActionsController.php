<?php

namespace DocumentManager\BackendBundle\Controller\Document_user;

use Admingenerated\DocumentManagerBackendBundle\BaseDocument_userController\ActionsController as BaseActionsController;
use Symfony\Component\HttpFoundation\RedirectResponse;
/**
 * ActionsController
 */
class ActionsController extends BaseActionsController
{
    /**
     * This is called when action is successfull
     * Default behavior is redirecting to list with success message
     *
     * @param \DocumentManager\BackendBundle\Entity\DocumentUser $DocumentUser Your \DocumentManager\BackendBundle\Entity\DocumentUser object
     * @return Response Must return a response!
     */
    protected function successObjectDelete(\DocumentManager\BackendBundle\Entity\DocumentUser $DocumentUser)
    {
        $document_id = $DocumentUser->getDocument()->getId();
        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans(
                "action.object.delete.success",
                array('%name%' => 'delete'),
                'Admingenerator'
            )
        );

        return new RedirectResponse($this->generateUrl("DocumentManager_BackendBundle_Document_user_list" , array('pk' => $document_id)));
    }
}
