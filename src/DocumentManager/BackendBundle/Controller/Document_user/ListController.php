<?php

namespace DocumentManager\BackendBundle\Controller\Document_user;

use Admingenerated\DocumentManagerBackendBundle\BaseDocument_userController\ListController as BaseListController;
use Symfony\Component\HttpFoundation\RedirectResponse;
/**
 * ListController
 */
class ListController extends BaseListController {

    public function indexAction() {
        $this->parseRequestForPager();

        $form = $this->getFilterForm();

        if ($this->getRequest()->get('pk')) {
            
            $pk = $this->getRequest()->get('pk');
            $em = $this->getDoctrine()->getManager();
            $document = $em->getRepository('DocumentManagerBackendBundle:Document')->find($pk);
        }

        return $this->render('DocumentManagerBackendBundle:Document_userList:index.html.twig', $this->getAdditionalRenderParameters() + array(
                    'DocumentUsers' => $this->getPager(),
                    'form' => $form->createView(),
                    'sortColumn' => $this->getSortColumn(),
                    'sortOrder' => $this->getSortOrder(),
                    'scopes' => $this->getScopes(),
                    'document' => $document
        ));
    }

    protected function getQuery() {

        $query = $this->getDoctrine()
                ->getManagerForClass('DocumentManager\BackendBundle\Entity\DocumentUser')
                ->getRepository('DocumentManager\BackendBundle\Entity\DocumentUser')
                ->createQueryBuilder('q');

        if ($this->getRequest()->get('pk')) {
            $pk = $this->getRequest()->get('pk');
            $em = $this->getDoctrine()->getManager();
            $document = $em->getRepository('DocumentManagerBackendBundle:Document')->find($pk);
            $query = $query->where('q.document = :document')
                    ->setParameter('document', $document);
        }
        $this->processQuery($query);
        $this->processSort($query);
        $this->processFilters($query);
        $this->processScopes($query);

        return $query->getQuery();
    }
    
    public function filtersAction()
    {
        if ($this->get('request')->get('reset')) {
            $this->setFilters(array());

            return new RedirectResponse($this->generateUrl("DocumentManager_BackendBundle_Document_user_list"));
        }

        if ($this->getRequest()->getMethod() == "POST") {
            $form = $this->getFilterForm();
            $form->bind($this->get('request'));

            if ($form->isValid()) {
                $filters = $form->getViewData();
            }
        }

        if ($this->getRequest()->getMethod() == "GET") {
            $filters = $this->getRequest()->query->all();
        }

        if (isset($filters)) {
            $this->setFilters($filters);
        }

        return new RedirectResponse($this->generateUrl("DocumentManager_BackendBundle_Document_user_list", array('pk' => $this->getRequest()->get('pk'))));
    }
}
