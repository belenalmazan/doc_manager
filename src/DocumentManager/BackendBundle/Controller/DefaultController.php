<?php

namespace DocumentManager\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('DocumentManagerBackendBundle:Default:index.html.twig', array());
    }
    
    public function downloadAction($filename)
    {
        $path = $this->container->getParameter('documents_upload_dir'); 
        $content = file_get_contents($path.DIRECTORY_SEPARATOR.$filename);

        $response = new Response();

        //set headers
        $response->headers->set('Content-Type', 'mime/type');
        $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename);

        $response->setContent($content);
        return $response;
    }
}

