<?php

namespace DocumentManager\BackendBundle\Form\Type\Document;

use Admingenerated\DocumentManagerBackendBundle\Form\BaseDocumentType\FiltersType as BaseFiltersType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * FiltersType
 */
class FiltersType extends BaseFiltersType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    
        $formOptions = $this->getFormOption('name', array(  'required' => false,  'label' => 'Name',  'translation_domain' => 'Admin',));
        $builder->add('name', 'text', $formOptions);

    
        $formOptions = $this->getFormOption('created', array(  
            'required' => false,  
            'label' => 'Created', 
            'translation_domain' => 'Admin', 
            'widget' => "single_text", 
            'format' => 'dd-MM-yyyy H:i:s',
            'attr' => array('class' => 'datetime')));
        $builder->add('created', 'datetime', $formOptions);
            
        $formOptions = $this->getFormOption('owner', array(  'multiple' => false,  'em' => 'default',  'class' => 'DocumentManager\\BackendBundle\\Entity\\User',  'required' => false,  'label' => 'Owner',  'translation_domain' => 'Admin',));
        $builder->add('owner', 'entity', $formOptions);

    
    }
}
