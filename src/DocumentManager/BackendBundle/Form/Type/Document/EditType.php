<?php

namespace DocumentManager\BackendBundle\Form\Type\Document;

use Admingenerated\DocumentManagerBackendBundle\Form\BaseDocumentType\EditType as BaseEditType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * EditType
 */
class EditType extends BaseEditType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $formOptions = $this->getFormOption('name', array('required' => true, 'label' => 'Name', 'translation_domain' => 'Admin',));
        $builder->add('name', 'text', $formOptions);

        $formOptions = $this->getFormOption('type', array('multiple' => false, 'em' => 'default', 'class' => 'DocumentManager\\BackendBundle\\Entity\\Type', 'required' => false, 'label' => 'Type', 'translation_domain' => 'Admin',));
        $builder->add('type', 'entity', $formOptions);

        $formOptions = $this->getFormOption('owner', array('multiple' => false, 'em' => 'default', 'class' => 'DocumentManager\\BackendBundle\\Entity\\User', 'required' => false, 'label' => 'Owner', 'translation_domain' => 'Admin',));
        $builder->add('owner', 'entity', $formOptions);

        $formOptions = $this->getFormOption('description', array('required' => false, 'label' => 'Description', 'translation_domain' => 'Admin', 'attr'=>array('rows' => '5', 'cols' =>'5' )));
        $builder->add('description', 'textarea', $formOptions);

        $formOptions = $this->getFormOption('path', array(  'required' => false,  'nameable' => 'documentFile',  'data_class' => 'Symfony\\Component\\HttpFoundation\\File\\File',  'label' => 'Document',  'translation_domain' => 'Admin',));
        $builder->add('path', 'afe_single_upload', $formOptions);
    }

}
