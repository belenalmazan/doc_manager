<?php

namespace DocumentManager\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DocumentUser
 *
 * @ORM\Table(name="document_user", indexes={@ORM\Index(name="fk_document_has_user_user1", columns={"user_id"}), @ORM\Index(name="fk_document_has_user_document1", columns={"document_id"})})
 * @ORM\Entity
 */
class DocumentUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DocumentManager\BackendBundle\Entity\Document
     *
     * @ORM\ManyToOne(targetEntity="DocumentManager\BackendBundle\Entity\Document")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="document_id", referencedColumnName="id")
     * })
     */
    private $document;

    /**
     * @var \DocumentManager\BackendBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="DocumentManager\BackendBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set document
     *
     * @param \DocumentManager\BackendBundle\Entity\Document $document
     * @return DocumentUser
     */
    public function setDocument(\DocumentManager\BackendBundle\Entity\Document $document = null)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return \DocumentManager\BackendBundle\Entity\Document 
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set user
     *
     * @param \DocumentManager\BackendBundle\Entity\User $user
     * @return DocumentUser
     */
    public function setUser(\DocumentManager\BackendBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \DocumentManager\BackendBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
