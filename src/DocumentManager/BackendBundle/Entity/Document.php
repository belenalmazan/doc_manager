<?php

namespace DocumentManager\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Document
 *
 * @ORM\Table(name="document", indexes={@ORM\Index(name="fk_document_type", columns={"type_id"}), @ORM\Index(name="fk_document_owner1", columns={"owner_id"})})
 * @ORM\Entity
 * @Vich\Uploadable
 */
class Document 
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     * @Assert\NotNull()
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;    

    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var datetime $updatedAt
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;
    
    /**
     * @Vich\UploadableField(mapping="document_file", fileNameProperty="documentFile", nullable=true)
     *
     * @var File $path
     * @Assert\NotNull()
     */ 
    private $path;
    
     /**
     * @ORM\Column(type="string", length=255, name="document_file", nullable=true)
     *
     * @var string $documentFile
     */
    protected $documentFile;
    
    /**
     * @var \DocumentManager\BackendBundle\Entity\Type
     *
     * @ORM\ManyToOne(targetEntity="DocumentManager\BackendBundle\Entity\Type")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=false)
     * })
     * @Assert\NotNull()
     */
    private $type;

    /**
     * @var \DocumentManager\BackendBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="DocumentManager\BackendBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="owner_id", referencedColumnName="id", nullable=false)
     * })
     * @Assert\NotNull()
     */
    private $owner;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Document
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set description
     *
     * @param string $description
     * @return Document
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }    

    /**
     * Get created
     * 
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }   

    /**
     * Set documentFile
     *
     * @param string $documentFile
     * @return DocumentFile
     */
    public function setDocumentFile($documentFile)
    {
        $this->documentFile = $documentFile;

        return $this;
    }

    /**
     * Get documentFile
     *
     * @return string 
     */
    public function getDocumentFile()
    {
        return $this->documentFile;
    }
    
    /**
     * Set path
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $path
     */
    public function setPath(File $path = null)
    {
        if(!is_null($path)){
            
            $this->path = $path;
            $this->setUpdatedAt(new \DateTime());  
            
        }        
        return $this;
    }

    /**
     * Get path
     *
     * @return File 
     */
    public function getPath()
    {
        return $this->path;
    }
    
    /**
     * Set type
     *
     * @param \DocumentManager\BackendBundle\Entity\Type $type
     * @return Document
     */
    public function setType(\DocumentManager\BackendBundle\Entity\Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \DocumentManager\BackendBundle\Entity\Type 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set owner
     *
     * @param \DocumentManager\BackendBundle\Entity\User $owner
     * @return Document
     */
    public function setOwner(\DocumentManager\BackendBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \DocumentManager\BackendBundle\Entity\User 
     */
    public function getOwner()
    {
        return $this->owner;
    }
    
    /**
     * Get updatedAt
     * 
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    } 
    
    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Document
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    } 
}
