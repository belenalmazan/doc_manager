<?php
namespace DocumentManager\BackendBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DocumentManager\BackendBundle\Entity\Type;

/**
 * Description of LoadType
 * 
 * @author belenalmazan
 */
class LoadType extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $types = array('Publico','Privado','Draft',);
    
        foreach($types as $type){
            $entity = new Type();
            $entity->setType($type);
            $manager->persist($entity);
        }
        $manager->flush();
    }
    
    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}
