<?php
namespace DocumentManager\BackendBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DocumentManager\BackendBundle\Entity\User;

/**
 * Description of LoadUser
 * 
 * @author belenalmazan
 */
class LoadUser extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $users = array('user1','user2','user3');
    
        foreach($users as $user){
            $entity = new User();
            $entity->setUsername($user);
            $manager->persist($entity);
        }
        $manager->flush();
    }
    
    public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }
}
